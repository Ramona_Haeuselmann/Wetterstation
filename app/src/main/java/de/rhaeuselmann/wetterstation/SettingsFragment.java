package de.rhaeuselmann.wetterstation;


import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/////////////////////////
////NICHT MEHR BENUTZT////
////////////////////////

public class SettingsFragment extends Fragment {
	
	public SettingsFragment(){}
	
//	public static SettingsFragment newInstance() {
//		return new SettingsFragment();
//	}
    	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
         
        return rootView;
    }
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       
        this.setRetainInstance(true);
     
	}
}
