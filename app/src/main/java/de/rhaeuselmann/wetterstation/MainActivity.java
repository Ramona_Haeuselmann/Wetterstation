package de.rhaeuselmann.wetterstation;

import java.util.ArrayList;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import de.rhaeuselmann.slidingmenu.adapter.NavDrawerListAdapter;
import de.rhaeuselmann.slidingmenu.model.NavDrawerItem;


public class MainActivity extends Activity 
	implements ConnectionFragment.ConnectionReady, DisplayValuesFragment.Ready {
	//implementiert das Interface "ConnectionReady" aus "ConnectionFragment"

    private BluetoothCommunication mChatService;
    private String display_values_fragment_tag;

    //Implementierung der Interface-Methoden
    //onConnectionReady und onDataReceived
	public void onConnectionReady(BluetoothCommunication ChatService){
		mChatService = ChatService;
//        DisplayValuesFragment measFrag = (DisplayValuesFragment)
//                getFragmentManager(). findFragmentByTag("android:switcher:2131361893:0");
//
//		if (measFrag != null) {
//            // Call a method in the ArticleFragment to update its content
//			measFrag.mChatService = ChatService;
//		}
	}
	
	public void onDataReceived(byte[] rData){
		//mChatService = ChatService;
		DisplayValuesFragment measFrag = (DisplayValuesFragment)
                getFragmentManager().findFragmentByTag(display_values_fragment_tag); //findFragmentByTag("android:switcher:2131361893:0");
		
		if (measFrag != null) {
            // Call a method in the ArticleFragment to update its content
            measFrag.storeAnswer(rData);
		}
	}

    public void onReadyToRefreshData(String tag)
    {
        display_values_fragment_tag = tag;
        DisplayValuesFragment measFrag = (DisplayValuesFragment)
                getFragmentManager().findFragmentByTag(display_values_fragment_tag); //findFragmentByTag("android:switcher:2131361893:0");

		if (measFrag != null) {
            // Call a method in the ArticleFragment to update its content
			measFrag.mChatService = mChatService;
		}
    }
	
	//private BluetoothChatService mChatService;

    //-------------------------------------------
    //--------ATTRIBUTE--------------------------
	//-------------------------------------------
	private Integer mNavSelected = -1; //aktuell selektiertes Menü
	
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
 
    // nav drawer title
    private CharSequence mDrawerTitle;
 
    // used to store app title
    private CharSequence mTitle;
 
    // slide menu items
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private TypedArray navMenuIconsSelected;

 
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;

    //Fragments
    private Fragment Connection = null;
    private Fragment Measurement = null;
    private Fragment Settings = null;
    private Fragment Help = null;
    private Fragment About = null;
    
    private Fragment lastFragment = null;
    //////////////////

    //----------END ATTRIBUTE--------------------
 
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); //Set the activity content to an explicit view
        
        //create all Fragments
        Connection = new ConnectionFragment();
        Measurement = new MeasureFragmentNeu();
        //Settings = new SettingsFragment();
        Settings = new SettingsPreferenceFragment();
        Help = new HelpFragment();
        About = new AboutFragment();

        //Zu Beginn einmal alle Fragmente anlegen und zum Fragment Manager hinzufügen
        //Fragmente hiden, dass sie später bei Bedarf geshowed oder gehided werden können
        if(savedInstanceState == null){
	        getFragmentManager().beginTransaction().add(R.id.frame_container, Connection).hide(Connection).commit();
	        getFragmentManager().beginTransaction().add(R.id.frame_container, Measurement,"measure_fragment").hide(Measurement).commit();
	        getFragmentManager().beginTransaction().add(R.id.frame_container, Settings).hide(Settings).commit();
	        getFragmentManager().beginTransaction().add(R.id.frame_container, Help).hide(Help).commit();
	        getFragmentManager().beginTransaction().add(R.id.frame_container, About).hide(About).commit();
        }
        
        
        ////////////////////////
        
 
        mTitle = mDrawerTitle = getTitle();
 
        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
 
        // nav drawer icons from resources
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);

        navMenuIconsSelected = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons_activated);
 
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
 
        navDrawerItems = new ArrayList<NavDrawerItem>();
 
        // adding nav drawer items to array
        // Connection
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        // Measurement
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        // Settings
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        // Help
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        // About
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));

        //refreshNavDrawerIcons(0);
 
        // Recycle the typed array
        //navMenuIcons.recycle();
 
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
 
        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);
 
        // enabling action bar app icon and behaving it as toggle button
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        //ActionBarDrawerToggle ties together the the proper interactions
        //between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_menu_white_24dp,//ic_menu_white_36dp,//ic_drawer, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                //calling onPrepareOptionsMenu() to show action bar icons: Prepare the Screen's standard options menu to be displayed. This is called right before the menu is shown, every time it is shown.
                invalidateOptionsMenu(); //Declare that the options menu has changed, so should be recreated.
            }
 
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
                invalidateOptionsMenu();
            }
        };

        refreshNavDrawerIcons(0);
        
        //Animation drawer icon
        mDrawerLayout.setDrawerListener(mDrawerToggle);
 
        if (savedInstanceState == null) {
            // on first time display view for first nav item
        	lastFragment = Connection;
        	mNavSelected = 0;
            displayView(0);
            
        }
    }
 
    /**
     * Slide menu item click listener
     * reagiert auf das Anklicken eines Elements des Navigation Drawers
     * -> Aufruf zur Anzeige des selektierten Menüelements
     * */
    private class SlideMenuClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id) {
        	mNavSelected = position;
            refreshNavDrawerIcons(position);
            // display view for selected nav drawer item
            displayView(position);
        }
    }

    /**
     * iconfarbe des selektierten Items im Navigation Drawer ändern
     * @param position selektiertes Item
     */

    private void refreshNavDrawerIcons(int position)
    {
        if(position == 0)
        {
            navDrawerItems.set(0, new NavDrawerItem(navMenuTitles[0], navMenuIconsSelected.getResourceId(0, -1)));
        }
        else
        {
            navDrawerItems.set(0, new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        }

        if(position == 1)
        {
            navDrawerItems.set(1, new NavDrawerItem(navMenuTitles[1], navMenuIconsSelected.getResourceId(1, -1)));
        }
        else
        {
            navDrawerItems.set(1, new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        }

        if(position == 2)
        {
            navDrawerItems.set(2, new NavDrawerItem(navMenuTitles[2], navMenuIconsSelected.getResourceId(2, -1)));
        }
        else
        {
            navDrawerItems.set(2, new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        }

        if(position == 3)
        {
            navDrawerItems.set(3, new NavDrawerItem(navMenuTitles[3], navMenuIconsSelected.getResourceId(3, -1)));
        }
        else
        {
            navDrawerItems.set(3, new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        }

        if(position == 4)
        {
            navDrawerItems.set(4, new NavDrawerItem(navMenuTitles[4], navMenuIconsSelected.getResourceId(4, -1)));
        }
        else
        {
            navDrawerItems.set(4, new NavDrawerItem(navMenuTitles[4], navMenuIcons.getResourceId(4, -1)));
        }


        adapter.setNavDrawerItems(navDrawerItems);

        //navMenuIconsSelected.recycle();
    }
 
    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }*/

    /**
     * Handle selections of MenuItems
     * @param item: selected item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        /*// Handle action bar actions click
        switch (item.getItemId()) {
        case R.id.action_settings:
            return true;
        default:
            
        }*/
        
        return super.onOptionsItemSelected(item);
    }
 
    /***
     * Called when invalidateOptionsMenu() is triggered
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
    	 // if nav drawer is opened, hide the action items
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        /*TODO
         * backstack back navigation navSelected
         */
        MenuItem myMenuItem = null;




        MeasureFragmentNeu measFragment = (MeasureFragmentNeu) getFragmentManager().findFragmentByTag("measure_fragment");


        if(measFragment.isHidden())
        {
            myMenuItem = menu.findItem(R.id.action_add_sensors);
            if (myMenuItem != null )
            {
                myMenuItem.setVisible(false);
            }

            myMenuItem = menu.findItem(R.id.action_refresh);
            if (myMenuItem != null ) {myMenuItem.setVisible(false);}
        }
        else
        {
            myMenuItem = menu.findItem(R.id.action_add_sensors);
            if (myMenuItem != null )
            {
                myMenuItem.setVisible(!drawerOpen);
            }

            myMenuItem = menu.findItem(R.id.action_refresh);
            if (myMenuItem != null ) {myMenuItem.setVisible(!drawerOpen);}
        }

        return super.onPrepareOptionsMenu(menu);
    }
 
    /**
     * Diplaying fragment view for selected nav drawer list item
     * */
    private void displayView(int position) {
        // update the main content by replacing fragments
        //Fragment fragment = null;
        //TODO back stack navigation richtig (selektierung in navigation drawer, manchmal überlagerung der fragmente)
    	
    	FragmentManager fragmentManager = getFragmentManager();
    	
        switch (position) {
        case 0:
            fragmentManager.beginTransaction()
            	.hide(lastFragment)
            	.show(Connection)
//            	.addToBackStack(null)
            	.commit();
            
            lastFragment = Connection;
        	//fragment = new ConnectionFragment();
            break;
        case 1:
        	fragmentManager.beginTransaction()
        	.hide(lastFragment)
        	.show(Measurement)
//        	.addToBackStack(null)
        	.commit();
        
        	lastFragment = Measurement;
            //fragment = new MeasureFragmentNeu();
            break;
        case 2:
        	fragmentManager.beginTransaction()
	        	.hide(lastFragment)
	        	.show(Settings)
//	        	.addToBackStack(null)
        		//.add(R.id.frame_container, Settings)
	        	.commit();
        	
        	
        
        	lastFragment = Settings;
            //fragment = new SettingsFragment();
            break;
        case 3:
        	fragmentManager.beginTransaction()
	        	.hide(lastFragment)
	        	.show(Help)
//	        	.addToBackStack(null)
	        	.commit();
        
        	lastFragment = Help;
            //fragment = new HelpFragment();
            break;
        case 4:
        	fragmentManager.beginTransaction()
	        	.hide(lastFragment)
	        	.show(About)
//	        	.addToBackStack(null)
	        	.commit();
        
        	lastFragment = About;
            //fragment = new AboutFragment();
            break;
        
 
        default:
            break;
        }
        
     // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerList.setSelection(position);
        setTitle(navMenuTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
 
        
        /*
        if (fragment != null) {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .addToBackStack(null)
                    .commit();
 
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(navMenuTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }*/
        
        
    }
 
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
 
    /**
     * When using the ActionBarDrawerToggle, you must call it during
     * onPostCreate() and onConfigurationChanged()...
     */
 
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }
 
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    
  //Drawer durch Druecken der Zuruecktaste schliessen
    @Override
    public void onBackPressed() {
    invalidateOptionsMenu();

    if(mDrawerLayout.isDrawerOpen(mDrawerList)){
        mDrawerLayout.closeDrawer(mDrawerList);
    }else{
        super.onBackPressed();
    }
    }






    @Override
    public void onPause() {
        super.onPause();


    }

    @Override
    public void onResume(){
        super .onResume();



    }



    @Override
    public void onDestroy() {
        super.onDestroy();


    }
    
 
}
