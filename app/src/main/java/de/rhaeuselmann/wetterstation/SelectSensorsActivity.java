package de.rhaeuselmann.wetterstation;


import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


public class SelectSensorsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Enable or disable the "home" button in the corner of the action bar.
        getActionBar().setHomeButtonEnabled(true);

        //Set whether home should be displayed as an "up" affordance.
        //Set this to true if selecting "home" returns up by a single level in your UI rather than back to the top level or front page.
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //Set whether to include the application home affordance in the action bar. Home is presented as either an activity icon or logo.
        getActionBar().setDisplayShowHomeEnabled(false); //false -> App Icon wird verborgen
        
        getActionBar().setTitle("  Sensoren wählen");
        //getActionBar().setIcon(R.drawable.ic_add_sensors);
        
     // Display the preferencefragment as the main content.
     getFragmentManager().beginTransaction()
    
            .replace(android.R.id.content, new SelectSensorsPreferenceFragment())
            .commit();
       
    }


   

    @Override
    public boolean onOptionsItemSelected(MenuItem item) 
    {    
       switch (item.getItemId()) 
       {        
          case android.R.id.home: //kleiner Pfeil links oben wirkt als "Zurück"-Button
        	  super.onBackPressed();          
             return true;        
          default:            
             return super.onOptionsItemSelected(item);    
       }
    }
}
