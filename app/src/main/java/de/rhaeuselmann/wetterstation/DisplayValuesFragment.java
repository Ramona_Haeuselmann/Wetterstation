package de.rhaeuselmann.wetterstation;

import android.app.Activity;
import android.app.Fragment;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

/**
 * Created by Ramona on 19.04.2015.
 * aktuelle Messwerte Fragment
 */
public class DisplayValuesFragment extends Fragment {

    //Member Variables
    private TextView mTempLabel;
    private TextView mTempValue;
    private TextView mTempUnit;
    private TextView mHumidityLabel;
    private TextView mHumidityValue;
    private TextView mHumidityUnit;
    private TextView mLuminosityLabel;
    private TextView mLuminosityValue;
    private TextView mLuminosityUnit;
    private TextView mRainLabel;
    private TextView mRainValue;

    private TextView mRefreshTime;
    private Time refreshTime = new Time();

    private ProgressBar RefreshValuesProgress;
    private Handler handler;


    private int commandID = -1;
    private boolean running = false;
    private static final long WAIT_MS = 50;

    private byte[][] data = new byte[4][3]; //Array of byte arrays für antwortbytes -> 4 byte arrays each of size 3
    private String[] interpretedData = new String[4];

    // Member object for the chat services
    public BluetoothCommunication mChatService = null;

    //Sensor Settings
    private boolean Temp;
    private boolean Humidity;
    private boolean Luminosity;
    private boolean Rain;

    //Data Store setting
    private boolean StoreData;


    /////////////////////////////////////

    Ready mCallback;

    //interface
    // Container Activity must implement this interface
    public interface Ready {
        public void onReadyToRefreshData(String tag);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (Ready) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ConnectionReady");
        }


    }




    public DisplayValuesFragment(){

    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        setHasOptionsMenu(true);
        handler = new Handler();
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.display_values_layout, null, false);

        setUIMembers(view);
        initUI();


//        mCallback.onReadyToRefreshData(getTag());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();  // Always call the superclass method first


    }

    private void initUI()
    {
        getSettings();
        showProgressBar(false);

        if(Temp)
        {
            showTempViews(true);
        }
        else
        {
            showTempViews(false);
        }

        if(Humidity)
        {
            showHumidityViews(true);
        }
        else
        {
            showHumidityViews(false);
        }

        if(Luminosity)
        {
            showLuminosityViews(true);
        }
        else
        {
            showLuminosityViews(false);
        }

        if(Rain)
        {
            showRainViews(true);
        }
        else
        {
            showRainViews(false);
        }
    }

    /**
     * UI Elemente zuweisen
     * @param view view
     */
    private void setUIMembers(View view)
    {
        mTempLabel = (TextView) view.findViewById(R.id.textViewTempLabel);
        mTempValue = (TextView) view.findViewById(R.id.textViewTempValue);
        mTempUnit = (TextView) view.findViewById(R.id.textViewTempUnit);
        mHumidityLabel = (TextView) view.findViewById(R.id.textViewLuftLabel);
        mHumidityValue = (TextView) view.findViewById(R.id.textViewLuftValue);
        mHumidityUnit = (TextView) view.findViewById(R.id.textViewLuftUnit);
        mLuminosityLabel = (TextView) view.findViewById(R.id.textViewLichtLabel);
        mLuminosityValue = (TextView) view.findViewById(R.id.textViewLichtValue);
        mLuminosityUnit = (TextView) view.findViewById(R.id.textViewLichtUnit);
        mRainLabel = (TextView) view.findViewById(R.id.textViewRegenLabel);
        mRainValue = (TextView) view.findViewById(R.id.textViewRegenValue);
        mRefreshTime = (TextView) view.findViewById(R.id.textViewRefreshTime);
        RefreshValuesProgress = (ProgressBar) view.findViewById(R.id.progressBarRefreshValues);

        //set color of progress bar
        RefreshValuesProgress.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.actionbar_color),
                android.graphics.PorterDuff.Mode.SRC_IN);
    }

    /**
     * progressbar (animierter kreis) anzeigen oder verbergen
     * @param show true anzeigen, false verbergen
     */
    private void showProgressBar(boolean show)
    {
        if(show){
            RefreshValuesProgress.setVisibility(View.VISIBLE);
        }
        else
        {
            RefreshValuesProgress.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Inflate the menu items for use in the action bar
        super.onCreateOptionsMenu(menu, inflater);
//        myMenu = menu;
        inflater.inflate(R.menu.measure_actions, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_refresh:
                mCallback.onReadyToRefreshData(getTag());

                if (mChatService != null)
                {
                    if (!running) {

                        for (int i = 0; i < 4; i++)
                        {
                            for(int j = 0; j < 3;j ++)
                            {
                                data [i][j] = (byte)0xFF;
                            }
                        }


                        new GetDataAsyncTask().execute("Start");
                    }
                }
                else
                {
                    Toast.makeText(getActivity().getApplicationContext(), "Keine Verbindung zur Wetterstation!", Toast.LENGTH_SHORT).show();
                }

                return true;

            case R.id.action_add_sensors:
                //preferences oeffnen -> SelectSensorsActivity starten
                Intent intent = new Intent(getActivity(), SelectSensorsActivity.class);
                startActivity(intent);

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void getSettings(){

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());

        Temp = sharedPref.getBoolean("sensor1", false);
        Humidity = sharedPref.getBoolean("sensor2", false);
        Luminosity = sharedPref.getBoolean("sensor3", false);
        Rain = sharedPref.getBoolean("sensor4", false);
        StoreData = sharedPref.getBoolean("log", false);
	}

    private class GetDataAsyncTask extends AsyncTask <String, Integer, String>
    {


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //displayProgressBar("Downloading...");
            showProgressBar(true);
            running = true;
            getSettings();
            writeRefreshTime();
        }

        @Override
        protected String doInBackground(String... params) {


            commandID = 0;
//            if(Temp) //Änderung: immer alle abfragen, da sonst zusätzliche Maßnahmen bei der Verarbeitung für das Diagramm ergriffen werden müssen (aus zeitlichen Gründen nicht)
            {
                getTemp();
                try {
                    Thread.sleep(WAIT_MS);
                }
                catch(InterruptedException ex){
                }
            }

            commandID = 1;
//            if(Humidity)
            {
                getHumidity();
                try {
                    Thread.sleep(WAIT_MS);
                }
                catch(InterruptedException ex){
                }
            }

            commandID = 2;
//            if(Luminosity)
            {
                getLuminosity();
                try {
                    Thread.sleep(WAIT_MS);
                }
                catch(InterruptedException ex){
                }
            }

            commandID = 3;
//            if(Rain)
            {
                getRain();
                try {
                    Thread.sleep(WAIT_MS);
                }
                catch(InterruptedException ex){
                }
            }

            commandID = 0;

            return "All Done!";

        }



        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //updateProgressBar(values[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //dismissProgressBar();

            if(interpretData() && StoreData)
            {
                storeDataToFile();
            }
            showData();
            showProgressBar(false);
            running = false;
        }

    }


    /**
     * fordet Temperatur von der Wetterstation
     */
    private void getTemp()
    {
        byte[] send = new byte[1];
        send[0] = 0x02;
        mChatService.write(send);
    }

    /**
     * fordert luftfeuchtigkeit von der Wetterstation
     */
    private void getHumidity()
    {
        byte[] send = new byte[1];
        send[0] = 0x01;
        mChatService.write(send);
    }

    /**
     * fordert Umgebungshelligkeit von der Wetterstation
     */
    private void getLuminosity()
    {
        byte[] send = new byte[1];
        send[0] = 0x03;
        mChatService.write(send);
    }

    /**
     * fordert Regen ja/nein von der Wetterstation
     */
    private void getRain()
    {
        byte[] send = new byte[1];
        send[0] = 0x00;
        mChatService.write(send);
    }

    /**
     * interpretiert die empfangenen Datenbytes
     */
    private boolean interpretData()
    {
        boolean gotTemp = false;
        boolean gotHumidity = false;
        boolean gotLuminosity = false;
        boolean gotRain = false;

        if(Temp)
        {
            gotTemp = interpretTemp();
        }

        if(Humidity)
        {
            gotHumidity = interpretHumidity();
        }

        if(Luminosity)
        {
            gotLuminosity = interpretLuminosity();
        }

        if(Rain)
        {
            gotRain = interpretRain();
        }

        return (gotTemp && gotHumidity && gotLuminosity && gotRain); //return true nur wenn von allen Antwort erhalten -> nur dann in File speichern
    }

    /**
     * interpretiert die Antwortbytes zu getTemp
     * und schreibt die interpretierten Daten als String in das StrinArray interpretedData
     */
    private boolean interpretTemp()
    {
        int sign;
        int vorkomma;
        int nachkomma;
        String answerString = "";

        sign = data[0][0];
        vorkomma = data[0][1];
        nachkomma = data[0][2];

        if(sign == -1 && vorkomma == -1 && nachkomma == -1)//keine Daten empfangen
        {
            interpretedData[0] = "--";
            return false;
        }


        if(sign == 16)// -> negativ
        {
            answerString = "- ";
        }

        answerString = answerString + Integer.toString(vorkomma) + "," + Integer.toString(nachkomma);
        interpretedData[0] = answerString;
        return true;
    }

    /**
     * interpretiert die Antwortbytes zu getHumidity
     * und schreibt die interpretierten Daten als String in das StrinArray interpretedData
     */
    private boolean interpretHumidity()
    {
        int vorkomma;
        int nachkomma;

        vorkomma = data[1][1];
        nachkomma = data[1][2];

        if(vorkomma == -1 && nachkomma == -1)
        {
            interpretedData[1] = "--";
            return false;
        }

        interpretedData[1] = Integer.toString(vorkomma) + "," + Integer.toString(nachkomma);
        return true;
    }

    /**
     * interpretiert die Antwortbytes zu getLuminosity
     * und schreibt die interpretierten Daten als String in das StrinArray interpretedData
     */
    private boolean interpretLuminosity()
    {
        int vorkomma;
        int nachkomma;
        double footcandle;
        double lux;

        vorkomma = data[2][1];
        nachkomma = data[2][2];

        if(vorkomma == -1 && nachkomma == -1)
        {
            interpretedData[2] = "--";
            return false;
        }

        footcandle = vorkomma + nachkomma/100;

        lux = footcandle * 10.76;

        interpretedData[2] = String.valueOf(lux).replace(".",",");
        return true;
    }

    /**
     * interpretiert die Antwortbytes zu getRain
     * und schreibt die interpretierten Daten als String in das StrinArray interpretedData
     */
    private boolean interpretRain()
    {
        int rainFlag;
        rainFlag = data[3][2];

        if(rainFlag == 16)// kein Regen
        {
            interpretedData[3] = "Nein";
        }
        else if( rainFlag == 32) //Regen
        {
            interpretedData[3] = "Ja";
        }
        else //keine Daten empfangen
        {
            interpretedData[3] = "--";
            return false;
        }
        return true;
    }


    /**
     * zeigt die Daten in der UI an
     */
    private void showData()
    {
        if(Temp)
        {
            showTempViews(true);
            mTempValue.setText(interpretedData[0]);
        }
        else
        {
            showTempViews(false);
        }

        if(Humidity)
        {
            showHumidityViews(true);
            mHumidityValue.setText(interpretedData[1]);
        }
        else
        {
            showHumidityViews(false);
        }

        if(Luminosity)
        {
            showLuminosityViews(true);
            mLuminosityValue.setText(interpretedData[2]);
        }
        else
        {
            showLuminosityViews(false);
        }

        if(Rain)
        {
            showRainViews(true);
            mRainValue.setText(interpretedData[3]);
        }
        else
        {
            showRainViews(false);
        }
    }

    /**
     * speichert die empfangenen Daten in array ab
     * @param answer antwortbytes
     */
    public void storeAnswer(byte[] answer)
    {
        data[commandID] = answer;
    }

    /**
     * Views der Temperaturanzeige anzeigen oder verbergen (Settings)
     * @param show true anzeigen, false verbergen
     */
    private void showTempViews(boolean show)
    {
        if(show)
        {
            mTempLabel.setVisibility(View.VISIBLE);
            mTempValue.setVisibility(View.VISIBLE);
            mTempUnit.setVisibility(View.VISIBLE);
        }
        else
        {
            mTempLabel.setVisibility(View.GONE);
            mTempValue.setVisibility(View.GONE);
            mTempUnit.setVisibility(View.GONE);
        }
    }

    /**
     * Views der Luftfeuchteanzeige anzeigen oder verbergen (Settings)
     * @param show true anzeigen, false verbergen
     */
    private void showHumidityViews(boolean show)
    {
        if(show)
        {
            mHumidityLabel.setVisibility(View.VISIBLE);
            mHumidityValue.setVisibility(View.VISIBLE);
            mHumidityUnit.setVisibility(View.VISIBLE);
        }
        else
        {
            mHumidityLabel.setVisibility(View.GONE);
            mHumidityValue.setVisibility(View.GONE);
            mHumidityUnit.setVisibility(View.GONE);
        }
    }

    /**
     * Views der Helligkeitsanzeige anzeigen oder verbergen (Settings)
     * @param show true anzeigen, false verbergen
     */
    private void showLuminosityViews(boolean show)
    {
        if(show)
        {
            mLuminosityLabel.setVisibility(View.VISIBLE);
            mLuminosityValue.setVisibility(View.VISIBLE);
            mLuminosityUnit.setVisibility(View.VISIBLE);
        }
        else
        {
            mLuminosityLabel.setVisibility(View.GONE);
            mLuminosityValue.setVisibility(View.GONE);
            mLuminosityUnit.setVisibility(View.GONE);
        }
    }

    /**
     * Views der Regenanzeige anzeigen oder verbergen (Settings)
     * @param show true anzeigen, false verbergen
     */
    private void showRainViews(boolean show)
    {
        if(show)
        {
            mRainLabel.setVisibility(View.VISIBLE);
            mRainValue.setVisibility(View.VISIBLE);
        }
        else
        {
            mRainLabel.setVisibility(View.GONE);
            mRainValue.setVisibility(View.GONE);
        }
    }

    /**
     * schreibt den Zeitpunkt der Aktualisierung der Messdaten
     */
    private void writeRefreshTime()
    {

        refreshTime.setToNow();
        mRefreshTime.setText("zuletzt aktualisiert: " + refreshTime.format("%H:%M:%S Uhr  %d.%m.%Y"));
	}

    /**
     * speichert zeitstempel und temperaturwert in file
     */
    private void storeDataToFile()
    {
        saveDatasetCounter();
        storeTempToFile();
        storeHumidityToFile();
        storeLuminosityToFile();
        storeRainToFile();
    }

    /**
     * schreibt Daten in eine Datei
     * @param filename Name der Datei
     * @param data zu speichernde Daten
     */
    private void writeToFile(String filename, String data)
    {
        File file = new File(getActivity().getFilesDir(), filename);

        try {
            //BufferedWriter for performance, true to set append to file flag
            BufferedWriter buf = new BufferedWriter(new FileWriter(file,true));
            buf.append(data);
            buf.newLine();
            buf.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void storeTempToFile()
    {
        String temp = interpretedData[0];
        long time = refreshTime.toMillis(true);

        String store = Long.toString(time) +";" + temp;

        writeToFile("Temp", store);
    }




    private void storeHumidityToFile()
    {
        String humidity = interpretedData[1];
        long time = refreshTime.toMillis(true);

        String store = Long.toString(time) +";" + humidity;

        writeToFile("Humidity", store);
    }

    private void storeLuminosityToFile()
    {
        String luminosity = interpretedData[2];
        long time = refreshTime.toMillis(true);

        String store = Long.toString(time) +";" + luminosity;

        writeToFile("Luminosity", store);
    }

    private void storeRainToFile()
    {
        String rain = interpretedData[3];
        long time = refreshTime.toMillis(true);

        if(rain.equals("Ja"))
        {
            rain = "1";
        }
        else if (rain.equals("Nein"))
        {
            rain = "0";
        }
        else
        {
            rain = null;
        }

        String store = Long.toString(time) +";" + rain;

        writeToFile("Rain", store);
    }




    private void saveDatasetCounter()
    {
        //get handle to shared preference
        Context context = getActivity();
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.dataset_counter_preference_file_key), Context.MODE_PRIVATE);

        //read from shared preference
        int counter = sharedPref.getInt(getString(R.string.dataset_counter), 0);

        counter++;

        //write to shared preference
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(getString(R.string.dataset_counter), counter);
        editor.commit();

    }




}




